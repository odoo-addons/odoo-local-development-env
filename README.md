# README

### setup

install `docker` and `docker-compose` first

### update modules

```
git submodule init
git submodule update

```


### run

```
./start.sh

```
